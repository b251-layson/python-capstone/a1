import gc
from abc import ABC, abstractclassmethod

members = []

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        pass


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def checkRequest(self):
        print(f"checked request")

    def addUser(self):
        print(f"Added user")

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")


class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def __str__(self):
        return f'{self._firstName} {self._lastName}'

    def checkRequest(self):
        print(f"checked request")

    def addUser(self):
        print(f"Added user")

    def login(self):
        print(f"Here {self._email} has logged in")

    def logout(self):
        print(f"Here {self._email} has logged out")

    def addMember(self, employeetobeadded):
        employeetobeadded.__class__ = TeamLead
        print(f"Successfully added {employeetobeadded._firstName} {employeetobeadded._lastName}")

    def get_members(self):
        for i in gc.get_objects():
            if isinstance(i, TeamLead):
                members.append(i)
        return members

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def checkRequest(self):
        print(f"checked request")

    def addUser(self):
        print(f"User has been added")

    def login(self):
        print(f"Here {self._email} has logged in")

    def logout(self):
        print(f"Here {self._email} has logged out")

    def addUser(self):
        print(f"User has been added")

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")

class Request():
    def __init__(self, name, requester, dateRequested, status):
        super().__init__()
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status

    def updateRequest(self):
        print(f"Updated request")

    def closeRequest(self):
        self._status = "closed"
        print(f"Request {self._name} has been {self._status}")

    def cancelRequest(self):
        self._status = "cancelled"
        print(f"Request {self._name} has been {self._status}")


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")

employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")

employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")

employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021", "On-going")

req2 = Request("Laptop repair", employee1, "1-Jul-2021", "On-going")

# assert employee1.getFullName() == "John Doe", "Full name should be John Doe"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp)

admin1.addUser()
req2.closeRequest()